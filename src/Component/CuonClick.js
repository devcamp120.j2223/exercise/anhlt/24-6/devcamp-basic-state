import { Component } from "react";
class CountClick extends Component{
    constructor(props){
        super(props);
        this.state = {
            count : 0
        }
        // cach 1
       // this.ClickHenderElement = this.ClickHenderElement.bind(this);
    }
    // ClickHenderElement(){
    //     this.setState({
    //         count: this.state.count + 1     
    //     })
    // }/
    // cach 2 arrow funtion không có this. nó chỉ chỏ ra đối tượng lớn nhất của moddul
    
    ClickHenderElement = () =>{
        this.setState({
            count: this.state.count + 1     
        })
    }
    render(){
        return(
            <div>
                <button onClick={this.ClickHenderElement}>Click Me!</button>
                <p>Coun Click : {this.state.count} Time</p>
            </div>
        )
    }
}
export default CountClick